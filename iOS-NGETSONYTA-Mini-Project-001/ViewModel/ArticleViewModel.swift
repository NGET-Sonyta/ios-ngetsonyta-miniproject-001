//
//  ArticleViewModel.swift
//  iOS-NGETSONYTA-Mini-Project-001
//
//  Created by Nyta on 12/12/20.
//

import Foundation

class ArticleViewModel : NSObject {
    
    private var apiService : ArticleService!
    private(set) var empData : Article! {
        didSet {
            self.bindEmployeeViewModelToController()
        }
    }
    
    var bindEmployeeViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  ArticleService()
        callFuncToGetEmpData()
    }
    
    func callFuncToGetEmpData() {
//        self.apiService.apiToGetEmployeeData { (empData) in
//            self.empData = empData
//        }
        self.apiService.fetchArticle(completionHandler: {(res) in
            self.empData = res
        })
    }
}
