
import Foundation
import UIKit


struct ViewModel {
   
    //Create singleton of View Model
    static let shared = ViewModel()
    
    
    //Fetch Data in View Model
    func fetchArticle(fetchPage: Int, completion: @escaping ([ArticleModel], [Int], Error?) -> ()){
        
        //Call Fetch Article from Service
        ArticleService.share.fetchArticle(fetchPage: fetchPage, completionHandler: {(data) in

            var articleModels = [ArticleModel]()
            
            for data in data.data{
                articleModels.append(ArticleModel(dataArticle: data))

            }
            completion(articleModels,[data.totalPage!, data.page ?? 1], nil)
        })
    }
    
    //Function Delete Article in View Model
    func deleteArticle(id: String, completion: @escaping (_ article: Article) -> ()){

        //Call Delete Article from Service
        ArticleService.share.deleteArticle(id: id, complitionHandler: {(data) in
        
        })
    }
    
    //Function Update Article in View Model
    func updateArticle(id: String, description: String, title: String, img: String, publish: Bool, completion: @escaping (_ article: Article) -> ()){
        
        //Call update Article from Service
        ArticleService.share.updateArticle(id: id, title: title, description: description, img: img, publish: publish, completionHandler: {(data) in
        })
    }
    
    //Function Post Article in View Model
    func postArticle(title: String, description: String, img: String, publish: Bool, completion: @escaping(_ article: Article) -> ()){
        
        //Call Post Article from Service
        ArticleService.share.postArticle(title: title, description: description, img: img, publish: true, completionHandler: {(res) in
            
        })
    }
 
    //Function Upload Images in View Model
    func uploadImgAPI(imgData: Data, completion: @escaping(_ articleResponse: ArticleResponse)-> ()) {
        
        //Call Upload Article from Service
        ArticleService.share.uploadImgAPI(imgData: imgData, completionHandler: {(res) in
           
            completion(res)
        })
    }
}
