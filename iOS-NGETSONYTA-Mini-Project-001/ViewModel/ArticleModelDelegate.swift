//
//  ArticleModelDelegate.swift
//  iOS-NGETSONYTA-Mini-Project-001
//
//  Created by Nyta on 12/13/20.
//

import Foundation

protocol ArticleModelDelegate{
    func didResponseArticle(_ articles: Article )
}
