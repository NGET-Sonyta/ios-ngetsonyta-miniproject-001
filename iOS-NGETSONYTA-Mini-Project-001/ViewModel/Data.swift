
import Foundation
import SwiftyJSON

//struct Data{
//    var id: String
//    var title: String
//    var image: String
//    var createdAt: String
//    var updatedAt: String
//    var description: String
//    
//    init(json: JSON) {
//        self.id = json["id"].stringValue
//        self.title = json["title"].stringValue
//        self.description = json["description"].stringValue
//        self.image = json["image"].stringValue
//        self.createdAt = json["createdAt"].stringValue
//        self.updatedAt = json["updatedAt"].stringValue
//        
//    }
//}

struct Article {
    var data : Data
    var limit : String?
    var page : String?
    var total_page: String?
    var message: String
    
    init(json: JSON){

        self.data = Data(json: json["data"])
        self.limit = json["limit"].stringValue
        self.total_page = json["total_page"].stringValue
        self.page = json["page"].stringValue
        self.message = json["message"].stringValue
    }
}
