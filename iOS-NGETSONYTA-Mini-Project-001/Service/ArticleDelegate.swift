//
//  ArticleDelegate.swift
//  iOS-NGETSONYTA-Mini-Project-001
//
//  Created by Nyta on 12/12/20.
//

import Foundation

protocol ArticleDelegate {
    
    func didResponseArticle(_ articles: Article )
}
