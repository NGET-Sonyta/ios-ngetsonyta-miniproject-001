
import Foundation
import Alamofire
import SwiftyJSON

class ArticleService{
    
    //Create singleton constant
    static let share = ArticleService()
    
    //main URL of Article
    let mainURL = "http://110.74.194.124:3000/api"
    
    //URL to upload images
    let uploadImgUR = "http://110.74.194.124:3000/api/images"
    
    //URL to post article
    let postURL = "http://110.74.194.124:3000/api/articles"
    
    //Function Fetch Article
    func fetchArticle(fetchPage: Int,completionHandler: @escaping(_ article: Article)-> Void){
        
        //Fetch article by pages
        AF.request("\(mainURL)/articles?pages=\(fetchPage)&size=15", method: .get).responseData{(response) in
            
            switch response.result{

            case .success(let value):

                do{
                    let decodeJson = try JSONDecoder().decode(Article.self, from: value)

                    completionHandler(decodeJson)
                    
                }catch let err{
                    print("Error catch: ",err)
                }

            case .failure(let err):
                print("Error Failure in Service", err)
            }
        }
    }
    

    //Function Delete Article
    func deleteArticle(id: String, complitionHandler: @escaping(_ article: Article) -> Void){

        AF.request("\(mainURL)/articles/\(id)", method:.delete).responseData{(response) in

        }
    }
    
    //Function Update Article
    func updateArticle(id: String, title: String, description: String, img: String, publish: Bool, completionHandler: @escaping(_ article: Article)-> Void){
        
        let headers : HTTPHeaders = ["Content-Type":"application/json"]
        let param: Parameters = [
          "title": title,
          "description": description,
          "published": true,
          "image": img
        ]
        
        AF.request("\(mainURL)/articles/\(id)", method: .patch, parameters: param, encoding: JSONEncoding.default, headers: headers ).responseJSON(completionHandler: {(res) in
            
            print("Update Response -------> ",res.result)
        })
    }
    
    //Function Post article
    func postArticle(title: String, description: String, img: String, publish: Bool, completionHandler: @escaping(_ article: Article)-> Void){
        
        let param: Parameters = [
          "title": title,
          "description": description,
          "published": true,
          "image": img
        ]
        
        AF.request(postURL, method: .post, parameters: param, encoding: JSONEncoding.default).responseJSON(completionHandler: {(res) in
 
            switch res.result{
            case .success(_):
                print("Upload Article in Service", res.result)
            case .failure(_):
                print("Fail to upload")
            }
            
        })
    }
    
    //Function upload images to API
    func uploadImgAPI(imgData:Data, completionHandler: @escaping(_ postArticle: ArticleResponse)-> Void) {
        
        AF.upload(multipartFormData:{ (formData)
            in
            formData.append(imgData, withName: "image", fileName: ".jpg" , mimeType: "image/jpeg")
        }, to: uploadImgUR, method:.post).responseData { (result) in
            
            switch result.result{

            case .success(let value):

                do{
                    let decodeJson = try JSONDecoder().decode(ArticleResponse.self, from: value)

                    completionHandler(decodeJson)

                }catch let err{
                    print("Error catch: ",err)
                }

            case .failure(let err):
                print("Error Failure in Service", err)
            }
        }
    }
}
    

