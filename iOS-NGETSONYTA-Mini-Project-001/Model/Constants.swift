//
//  Constants.swift
//  iOS-NGETSONYTA-Mini-Project-001
//
//  Created by Nyta on 12/12/20.
//

import Foundation

enum Config{
    
    private static let MAIN_URL = "http://110.74.194.124:3000"
    
    static let FETCH_URL = MAIN_URL + "/api/articles"
}
