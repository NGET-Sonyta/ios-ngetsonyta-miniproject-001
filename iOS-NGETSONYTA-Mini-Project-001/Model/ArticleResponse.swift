
import Foundation

//Create struct when get url after uplaod images to API
struct ArticleResponse: Codable{
    var version: Int
    var id: String
    var createdAt : String
    var updatedAt: String
    var url: String
    
    enum CodingKeys: String, CodingKey {
            case version = "__v"
            case id = "_id"
            case createdAt
            case updatedAt
            case url
    }
    
}
