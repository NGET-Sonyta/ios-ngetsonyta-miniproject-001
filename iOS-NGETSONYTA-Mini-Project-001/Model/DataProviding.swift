//import Foundation
//
//// MARK: - DataProviding
//struct DataProviding: Codable {
//    let data: [Article]
//    let page, limit, totalPage: JSONNull?
//    let message: String
//
//    enum CodingKeys: String, CodingKey {
//        case data, page, limit
//        case totalPage = "total_page"
//        case message
//    }
//}
//
//
//
//
//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
