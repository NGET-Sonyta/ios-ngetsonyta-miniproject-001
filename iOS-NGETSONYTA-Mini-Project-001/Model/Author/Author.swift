import Foundation

//Author Model
struct Author: Codable{
    var id: String
    var name: String
    var image: String
    var email: String
    
    enum CodingKeys: String, CodingKey {
            case id = "_id"
            case name, email, image
    }
}
