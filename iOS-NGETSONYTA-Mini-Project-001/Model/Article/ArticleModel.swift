
import Foundation
import UIKit

//Customize Article Model
struct ArticleModel {
    
    //Data Needed from Article
    var id: String
    var title: String
    var description : String
    var createAt: String
    var image: String
    var author: String
    var type: String
    var authorImg: UIImage
    
    

    init(dataArticle: DataArticle){
        
        //Create constant to random
        let name: [String] = ["Christ Lattner", "John Gosling", "Polar Bear", "Rosie is Rosie", "Ariana Grade"]
        
        let type: [String] = ["General", "Entertainment", "Education", "Sport", "Health"]
        
        let image: [UIImage] = [
                UIImage(named: "pf1")!,
                UIImage(named: "logoApp")!,
                UIImage(named: "myProfile")!,
                UIImage(named: "myProfile1")!
        ]
        
        self.id = dataArticle.id
        self.title = dataArticle.title
        self.description = dataArticle.datumDescription
        self.createAt = dataArticle.createdAt
        self.authorImg = image.randomElement()!
        self.author = name.randomElement()!
        self.type = type.randomElement()!
        self.image = dataArticle.image ?? "String Image"
        
        //Get format Date from API
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let date = dateFormat.date(from: dataArticle.createdAt){
            let newDate = DateFormatter()
            newDate.dateFormat = "dd-MMM-yyyy"
            self.createAt = "Date: \(newDate.string(from: date))"
        }
    }
}
