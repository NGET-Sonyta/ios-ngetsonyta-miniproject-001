
import Foundation

//Article Model Generate with QuickType
struct Article: Codable {
    let data: [DataArticle]
    let page: Int?
    let limit: Int?
    let totalPage: Int?
    let message: String

    enum CodingKeys: String, CodingKey {
        case data
        case page
        case limit
        case totalPage = "total_page"
        case message
    }
}

struct DataArticle: Codable {
    let id, title, datumDescription: String
    let published: Bool?
    let image: String?
    let createdAt, updatedAt: String
    let v: Int
    let category: Category?
    let author: Author?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title
        case datumDescription = "description"
        case published, image, createdAt, updatedAt
        case v = "__v"
        case category
        case author
    }
}

struct Category: Codable {
    let id, name, createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, createdAt, updatedAt
        case v = "__v"
    }
}


class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

