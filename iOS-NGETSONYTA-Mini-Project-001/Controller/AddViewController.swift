
import UIKit

class AddViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate, UIGestureRecognizerDelegate, DelegateData {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    
    //Create variable as type "DelegateData"
    var delegateEdit: DelegateData?
    
    let network = ViewModel.shared
    
    //Declare empty variable to receive from delegate
    var descriptionEdit = ""
    var titleEdit = ""
    var action = ""
    var photo = ""
    var idEdit = ""
    
    //Declare variable as false to check whether the users click on photo
    var isImgSelected : Bool = false
    
    //Create variable from UIImagePickerController
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Change shape of Elements
        imageView.layer.cornerRadius = 15
        txtDescription!.layer.borderWidth = 0.5
        txtDescription!.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.cornerRadius = 10
        
        //Subscribe to Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceive), name: Notification.Name("ColorChange"), object: nil)
        
        // create tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(gesture:)))
        
        // add it to the image view
        imageView.addGestureRecognizer(tapGesture)
        
        // make sure imageView can be interacted with by user
        imageView.isUserInteractionEnabled = true

        imagePicker.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
                
        //Check on action update and save
        if action == "update"{
            
            //Receive text from record to allow updating
            txtTitle.text = titleEdit
            txtDescription.text = descriptionEdit
            
            //Set title button to "Update"
            btnSave.setTitle("Update".localizedString(), for: .normal)
            
            //Set navigation Title to Update
            self.navigationItem.title = "UpdateArticle".localizedString()
            
            //Display image
            imageView.kf.setImage(with: URL(string: photo ), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil) { result in
                        switch result {
                        case .success( _):
                            break
                        case .failure(let error):
                            if error.isInvalidResponseStatusCode {
                                self.imageView.image = UIImage(named: "default")
                            }
                    }
                }
        }else{
            
            //Set button title to "Save"
            btnSave.setTitle("Save".localizedString(), for: .normal)
            
            //Set navigation Title
            self.navigationItem.title = "AddArticle".localizedString()
        }
        
        //Set text lable
        lblTitle.text = "Title".localizedString()
        lblDescription.text = "Description".localizedString()

    }
    
    //Receive Notification Center
    @objc func notificationReceive() {
        view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
        txtDescription.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
       
    }
    
    //Function when users tap on images
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                   
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //Function when users pick images from gallary
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            self.imageView.image = image
            isImgSelected = true
            
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    //Function upload images to API
    func uploadImg() {
        let imgData = UIImage.jpegData(self.imageView.image!) (compressionQuality: 0.5)
          
        //Check condition if user pick images
        if isImgSelected {
            self.network.uploadImgAPI(imgData: imgData!, completion: { [self](res)
                   in
               
               //Pass url image to add function
            add(s: res.url)
            
            })
        }else{
              
            //Pass url as "String Image" in case users do not pick images
            add(s: "String Image")
          }
      }
    
    @IBOutlet weak var button: UIButton!
    
    func passData(title: String, description: String, date: String, image: String, profile: UIImage, username: String, id: String, checkAction: String) {
        
        action = checkAction
        titleEdit = title
        descriptionEdit = description
        photo = image
        idEdit = id
        
    }
    
    
    @IBAction func btnSave(_ sender: Any) {

        //Button do action according to Update & Save
        if action == "update"{
            update()
            alertUpdate()
            
        }else{
            uploadImg()
            
        }
            
    }
    
    //Function Add Article
    func add( s : String) {
                
        //Call function Post Article regarding to user's input
        self.network.postArticle(title: txtTitle.text ?? "Title", description: txtDescription.text, img: s, publish: true, completion: {(res) in
                
        })
        
        //Create Alert
        let alert = UIAlertController(title: nil, message: "Added".localizedString(), preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: "Ok".localizedString(), style: .default, handler: { _ in
                
            //Set everything to empty after users done adding article
            self.txtTitle.text = ""
            self.txtDescription.text = ""
            self.isImgSelected = false
            self.imageView.image = UIImage(named: "default")
               
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
  
    //Function Update Article
    func update(){
        
        //Get images from UIImage as Data
       let imgData = UIImage.jpegData(self.imageView.image!) (compressionQuality: 0.5)
        
       //Upload images to API
       self.network.uploadImgAPI(imgData: imgData!, completion: { [self](res)
            in

        //Get Url Images after upload and pass via parameter
        update(s: res.url)
        
       })
    }
    
    
    //Function Update Article
    func update(s: String){
        
        //Display text in TextBox to update
        let title = txtTitle.text!
        let description = txtDescription.text!
    
        //Call Function Update Artcile to update to API
        self.network.updateArticle(id: idEdit, description: description, title: title, img: s, publish: true, completion: {(res) in
            
        })
    }

    //Create function to alert after user update Article
    func alertUpdate(){
     
        let alert = UIAlertController(title: nil, message: "Added".localizedString(), preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: "Ok".localizedString(), style: .default, handler: { _ in
                self.navigationController!.popToRootViewController(animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
    }
}
    
