//
//  CardCollectionViewCell.swift
//  iOS-NGETSONYTA-Mini-Project-001
//
//  Created by Nyta on 12/8/20.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    //Create static identifier
    static var identifier = "cardCell"
    
    @IBOutlet weak var cardType: UILabel!
    @IBOutlet weak var cardCategory: UILabel!
    @IBOutlet weak var cardDate: UILabel!
    @IBOutlet weak var cardUsername: UILabel!
    @IBOutlet weak var cardCaption: UILabel!
    @IBOutlet weak var cardProfile: UIImageView!
    @IBOutlet weak var cardImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        cardProfile.layer.cornerRadius = cardProfile.frame.height/2
    }
    
    //Create static function of UINib
    static func nib() -> UINib {
        return UINib(nibName: "CardCollectionViewCell", bundle: nil)
    }
    
    //Create func to pass data from UINib to View Controller
    func configureCard(username: String, profile: UIImage, caption: String, image: UIImage, type: String, category: String, date: String ){
        
        self.cardUsername.text = username
        self.cardProfile.image = profile
        self.cardCaption.text = caption
        self.cardCaption.text = caption
        self.cardImage.image = image
        self.cardType.text = type
        self.cardCategory.text = category
        self.cardDate.text = date
       
        
    }

}
