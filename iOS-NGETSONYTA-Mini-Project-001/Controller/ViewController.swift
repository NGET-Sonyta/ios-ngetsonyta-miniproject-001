import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class ViewController: UIViewController {

    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var search: UIButton!
    @IBOutlet weak var searchbtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var happy: UILabel!
    @IBOutlet weak var lonely: UILabel!
    @IBOutlet weak var worry: UILabel!
    @IBOutlet weak var sad: UILabel!
    @IBOutlet weak var angry: UILabel!
    
    //Set Title of Navigation Title
    var titleNav: String = "Popular Article".localizedString()
    
    //Create variable as DelegateData
    var delegateData: DelegateData?
    
    //Create instace from Article Model
    var articles = [ArticleModel]()
    
    let network = ViewModel.shared
    
    //Create varible to false as default
    var isFetchMore: Bool = false
    var page: Int = 1
    var modelPage = [Int]()
    
    var refreshControl = UIRefreshControl()
    
    //Create Empty Variable to delegate
    var deleteID: String = ""
    var caption: String = ""
    var descriptionDele: String = ""
    var username: String = ""
    var date: String = ""
    var id: String = ""
    var photo: String = ""
    
    
    private var articleViewModel : ArticleModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = titleNav
        
        //Reload Table View
        self.tableView.reloadData()
        
        //Fetch Data to Display
        fetchData()
        
        //Register Cell from Nib file
        registerCell()
        
        //Change shape/color of elements
        search.layer.cornerRadius = 5
        UITabBar.appearance().barTintColor = UIColor.blue
        self.tableView.separatorColor = .white
        
        //Hide scroll indicator
        tableView.showsVerticalScrollIndicator = false
        
        //Refresh Data
        tableView.refreshControl = UIRefreshControl()
        
        //Add Refresh Target
        tableView.refreshControl?.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)
        
        //Subscribe Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceive), name: Notification.Name("ColorChange"), object: nil)
        
    }
    
    //Refresh Target
    @objc func didPullRefresh() {
            fetchData()
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
            }
    }
    
    // Every time screen appears
    override func viewWillAppear(_ animated: Bool) {
        
        //Reload Data
        self.tableView.reloadData()
        
        //Localize Text
        title = "Article".localizedString()
        worry.text = "Worry".localizedString()
        angry.text = "Angry".localizedString()
        happy.text = "Happy".localizedString()
        sad.text = "Sad".localizedString()
        lonely.text = "Lonely".localizedString()
        
    }
    
    //Receive Notification Center
    @objc func notificationReceive() {
        view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
        tableView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
        stack.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
    }
    
    //Function Register Cell
    func registerCell(){
        tableView.register(UINib(nibName: "CardTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")

    }
    
    //Function Fetch Data with Page
    func fetchData(fetchPage: Int? = 1){

        //Call function fetch data with MVVM Pattern
        network.fetchArticle(fetchPage: fetchPage!,completion: {(res,arrPage, error) in

            //Check condition to fecth data
            if self.isFetchMore{
                
                //Append Old Data with new data to display
                self.articles.append(contentsOf: res)
            }else{
                
                //When article reach out to nth to fetch anymore
                self.articles = res
            }
            
            self.modelPage = arrPage
            self.tableView.reloadData()
            
            //Stop refreshing
            self.tableView.refreshControl?.endRefreshing()
        })
      
      
    }

    
    //Alert function Delete
    func alertFunc(){
        let alert = UIAlertController(title: nil, message: "The record has been sucessfully deleted.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                      
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource{
    
    //Display Data
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //Create variable item
        let item = articles.count - 1
        
        //Check condition of item
        if item == indexPath.row{
            
            //Break if it reachs the end
            if modelPage[0] < modelPage[1]{
                
                return
                
            }else{
                
                //Fetch data more if it does not reach the end
                self.isFetchMore = true
                page = page + 1
                fetchData(fetchPage: page)
                
            }
           
        }
    }

    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {

        
        // Pass the indexPath as the identifier for the menu configuration
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            
            //Context Menu of edit
            let edit = UIAction(title: "Edit".localizedString(), image: UIImage(systemName: "note.text")) { _ in
                
                //Create instance of Add VC
                let vc = (self.storyboard?.instantiateViewController(withIdentifier: "AddViewController")) as! AddViewController
                
                //Delegate to that VC
                self.delegateData = vc
                
                //Delegate via function "passData"
                self.delegateData?.passData(title: self.articles[indexPath.row].title, description: self.articles[indexPath.row].description, date: self.date, image: self.articles[indexPath.row].image, profile: self.articles[indexPath.row].authorImg, username: self.articles[indexPath.row].author, id: self.articles[indexPath.row].id, checkAction: "update")
                
                //Push to View Controller
                self.navigationController?.pushViewController(vc, animated: true)
                }

            //Context Menu of Delete
            let delete = UIAction(title: "Delete".localizedString(), image: UIImage(systemName: "trash"), attributes: .destructive) { _ in
                
                //Get if of article to Delete
                self.deleteID = self.articles[indexPath.row].id
                
                //Pass that ID to Function Delete with MVVM Pattern
                self.network.deleteArticle(id: self.deleteID, completion: {(res) in
                    
            })
                
                DispatchQueue.main.async {
                    
                    //Reload Data table without waiting other threads
                    self.tableView.reloadData()
                }
               
                //Call function Alert ("Delete")
                self.alertFunc()
                
                //Remove that article from table
                self.articles.remove(at: indexPath.row)
    }

            //Context Menu of share
            let shareAction = UIAction(title: "Share".localizedString(), image: UIImage(systemName: "square.and.arrow.up")) { _ in
                        print("Share")
                }

            return UIMenu(title: "", children: [ edit, delete, shareAction])
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return articles.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let dataID = articles[indexPath.row].id
        let dataTitle = articles[indexPath.row].title
        let dataDescription = articles[indexPath.row].description
        let dataDate = articles[indexPath.row].createAt
        let dataUsername = articles[indexPath.row].author
        let dataprofile = articles[indexPath.row].authorImg
        let img = articles[indexPath.row].image
        
        //Create instance of Detail VC
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController")) as! DetailViewController
        
        //Delegate Data
        self.delegateData = vc as DelegateData
        delegateData?.passData(title: dataTitle, description: dataDescription, date: dataDate, image: img, profile: dataprofile, username: dataUsername, id: dataID, checkAction: "detail")

        //Push to Detail VC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Register Cell with identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardTableViewCell
        
        //Get each data of Articles
        let randomImage = articles[indexPath.row].authorImg
        let randomType = articles[indexPath.row].type
        photo = articles[indexPath.row].image
        caption = articles[indexPath.row].title
        id = articles[indexPath.row].id
        date = articles[indexPath.row].createAt
        descriptionDele = articles[indexPath.row].description

        //Pass data to nib file by function ConfigureCard
        cell.configureCard(username: articles[indexPath.row].author, profile: randomImage, caption: caption, type: "General", category: randomType, date: date, img: photo)
            return cell
    }

}

// Create protocol to delegate
protocol DelegateData {
    
    // Pass data to another Controller via parameters
    func passData(title: String, description: String, date: String, image: String, profile: UIImage, username: String, id: String, checkAction: String)
}

