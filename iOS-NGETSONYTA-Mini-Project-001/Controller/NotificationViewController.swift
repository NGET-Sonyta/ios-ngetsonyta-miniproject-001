

import UIKit

class NotificationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Subscribe to Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceive), name: Notification.Name("ColorChange"), object: nil)
        
    }
    
    //Set background color to light purple
    @objc func notificationReceive() {
        view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Dynamic language when users switch languages
        title = "Notication".localizedString()
    }

}

