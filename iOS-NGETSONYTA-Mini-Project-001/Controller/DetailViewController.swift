
import UIKit
import Kingfisher


class DetailViewController: UIViewController {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var des: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let network = ViewModel.shared
    var articleViewModel : ViewModel?
    
    //Declare emplty variable to get data when delegate from other VC
    var myTitle: String = ""
    var myDescription: String = ""
    var myDate: String = ""
    var myImage: UIImage?
    var myProfile: UIImage?
    var myUsername: String = ""
    var myID: String = ""
    var pic: String = ""
    
    //Create variable to Delegate data
    var delegateUpdate: DelegateData?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        //Hide Scroll Indicator
        scrollView.showsVerticalScrollIndicator = false
        
        //Set shape of images
        self.profile.layer.cornerRadius = profile.frame.height/2
        img.layer.cornerRadius = 25
        
        //Get Data when delegate
        caption.text = myTitle
        name.text = myUsername
        date.text = myDate
        des.text = myDescription
        img.image = myImage
        profile.image = myProfile
        
        //Display Images with KingFisher
        img.kf.setImage(with: URL(string: pic ), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil) { result in
                    switch result {
                    case .success( _):
                        break
                    case .failure(let error):
                        if error.isInvalidResponseStatusCode {
                            self.img.image = UIImage(named: "default")
                        }
                    }
                }
    }
    
    //Create func to naviagte to Root Controller
    func naviagteToRoot(){
        self.navigationController!.popToRootViewController(animated: true)
    }

    @IBAction func btnDelete(_ sender: Any) {
       
        // Create alert
        let alert = UIAlertController(title: nil, message: "DeleteSure".localizedString(), preferredStyle: UIAlertController.Style.alert)
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: "Delete".localizedString(), style: .destructive, handler: { _ in
            
            //Function to delete article by ID
            self.network.deleteArticle(id: self.myID, completion: {(res) in
                
                //Alert for Delete
                let subAlert = UIAlertController(title: "Notication".localizedString(), message: "DeleteSuccessfully".localizedString(), preferredStyle: UIAlertController.Style.alert)
                subAlert.addAction(UIAlertAction(title: "GotIt".localizedString(), style: .default, handler: { _ in
                    
                }))
                
                self.present(subAlert, animated: true, completion: nil)
            })
            
            self.naviagteToRoot()
        }))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: "Cancel".localizedString(), style: .cancel,
        handler: {(_: UIAlertAction!) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func btnEdit(_ sender: Any) {
   
        //Create instantiate View Controller
        let vcUpdate = (self.storyboard?.instantiateViewController(withIdentifier: "AddViewController")) as! AddViewController
        
        //Delegate to instantiate View Controller
        self.delegateUpdate = vcUpdate
        
        //Delegate data with function "passData"
        delegateUpdate?.passData(title: myTitle, description: myDescription, date: myDate, image: pic, profile: (img.image ?? UIImage(named: "default"))!, username: "1", id: myID, checkAction: "update")
        
        //Push to AddViewController
        self.navigationController?.pushViewController(vcUpdate, animated: true)
    }
}

extension DetailViewController : DelegateData{
    
    //Function of delegate which conforms from protocol DelegateData
    func passData(title: String, description: String, date: String, image: String, profile: UIImage, username: String, id: String, checkAction: String) {
        myTitle = title
        myDescription = description
        myUsername = username
        myDate = date
        myProfile = profile
        myID = id
        pic = image
    }
    
}

