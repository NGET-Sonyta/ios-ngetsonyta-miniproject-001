
import UIKit

class UserViewController: UIViewController {

    @IBOutlet weak var switchLang: UISwitch!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var eng: UILabel!
    @IBOutlet weak var kh: UILabel!
    @IBOutlet weak var themeColor: UILabel!
    @IBOutlet weak var numberPost: UILabel!
    @IBOutlet weak var totalPost: UILabel!
    @IBOutlet weak var myProfile: UIImageView!
    @IBOutlet weak var myName: UILabel!
    @IBOutlet weak var color1: UIView!
    @IBOutlet weak var color2: UIView!
    @IBOutlet weak var color3: UIView!
    @IBOutlet weak var color4: UIView!
    @IBOutlet weak var color5: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var btnLogOut: UIButton!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var languageView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Check condition on "Switch"
        if AppService.shared.language == "en" {
            switchLang.isOn = true
        } else {
            switchLang.isOn = false
                
        }
        
        //Call function to change shapes
        changeShape()
        
        //Create tapGesture as an instance of UITapGestureRecoginzer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(colorTapped(gesture:)))
        
        //Add Gesture to color1
        color1.addGestureRecognizer(tapGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Dynamic language every time screen appears
        changeLanguae()
    }
    
    
    //Objective-C function
     @objc func colorTapped(gesture: UIGestureRecognizer) {
        
        //Post Notification Center to change colo
        NotificationCenter.default.post(name: Notification.Name("ColorChange"),     object: nil)
        
        view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
        myView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
    }
    
    
    @IBAction func logOut(_ sender: UIButton) {
        
        
        //Create Alert LogOut
        let alert = UIAlertController(title: nil, message: "AreYouSuretoLogOut".localizedString(), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Sure".localizedString(), style: .destructive, handler: { _ in
                                      
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localizedString(), style: .default, handler: { _ in
                                      
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //Function to change shape of elements
    func changeShape(){
        
        color1.layer.cornerRadius = color1.frame.height/2
        color2.layer.cornerRadius = color2.frame.height/2
        color3.layer.cornerRadius = color3.frame.height/2
        color4.layer.cornerRadius = color4.frame.height/2
        color5.layer.cornerRadius = color5.frame.height/2
        myProfile.layer.cornerRadius = myProfile.frame.height/2
        colorView.layer.cornerRadius = 15
        postView.layer.cornerRadius = 15
        languageView.layer.cornerRadius = 15
        btnLogOut.layer.cornerRadius = 10
        
    }
    
    
    // Create function Set Language
    func setLang(lang: String){
        AppService.shared.choose(language: lang)
    }
    
    //Function Change Language
    func changeLanguae(){
        
        myName.text = "Sonyta".localizedString()
        themeColor.text = "Theme".localizedString()
        language.text = "Language".localizedString()
        eng.text = "Eng".localizedString()
        kh.text = "Kh".localizedString()
        numberPost.text = "NumberPost".localizedString()
        totalPost.text = "TotalPost".localizedString()
        self.title = "Profile".localizedString()
        btnLogOut.setTitle("LogOut".localizedString(), for: .normal)
    }

    
    @IBAction func mySwitch(_ sender: UISwitch) {
        
        //Check condition of UISwitch
        if sender.isOn{
            
            setLang(lang: languageLocalize.english.rawValue)
            changeLanguae()
            
        }else{
           
            setLang(lang: languageLocalize.khmer.rawValue)
            changeLanguae()
        }
    }
}

//Extension to swicth Language
extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: self)
    }
}
