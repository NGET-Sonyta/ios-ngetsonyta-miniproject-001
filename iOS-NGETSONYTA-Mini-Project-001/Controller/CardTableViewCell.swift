
import UIKit
import Kingfisher

class CardTableViewCell: UITableViewCell {

 
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var type: UILabel!

    //Create static identifier
    static var identifier = "cardCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    //Subscribe Notification Observer when users change backgroundcolor
    NotificationCenter.default.addObserver(self, selector: #selector(notificationReceive), name: Notification.Name("ColorChange"), object: nil)
    
    //Adjust shape/size of images
    myView.layer.cornerRadius = 20
    profile.layer.cornerRadius = profile.frame.height/2
    img.layer.cornerRadius = 20
        
    }
    
    //Set background color when users click "Change Theme Color"
    @objc func notificationReceive() {
        myView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9058823529, blue: 0.9647058824, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //Create Static Nib Function
    static func nib() -> UINib {
        return UINib(nibName: "CardTableViewCell", bundle: nil)
    }
    
    //Create func to pass data from UINib to View Controller
    func configureCard(username: String, profile: UIImage, caption: String, type: String, category: String, date: String, img: String){

        
        self.username.text = username
        self.profile.image = profile
        self.caption.text = caption
        self.type.text = type
        self.category.text = category
        self.date.text = date
        let url = URL(string: img)

        //Show images with KingFisher
        self.img.kf.setImage(with: url, placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (response) in
            
            switch response{
            case .success(_):
                break
            case .failure(_):
                self.img.image = UIImage(named: "default")
            }
        })
    }
}
