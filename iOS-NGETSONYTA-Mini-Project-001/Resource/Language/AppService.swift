import Foundation
import UIKit

struct AppService {
    
    static let shared = AppService()
    
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func choose(language: String){
        UserDefaults.standard.set(language, forKey: "lang")
    }
}

enum languageLocalize: String {
    case english = "en"
    case khmer = "km-KH"
}


